package qa.tests;

import business.objects.AddProductToCart;
import business.objects.BasketAction;
import business.objects.Navigate;
import com.persado.oss.quality.stevia.spring.SteviaTestBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.Amazon;

public class AmazonCartTests extends SteviaTestBase {

    @Autowired
    protected Navigate navigate;
    @Autowired
    protected AddProductToCart addProductToCart;
    @Autowired
    protected BasketAction basketAction;


    @Test
    public void item_price_in_cart_should_remain_as_expected() {

        navigate.To(Amazon.Pages.Mens_Dress_Shoes);
        String item_expected_price = addProductToCart.selectFromListProductAt(3).setSize("10.5").add().getPrice();

        navigate.To(Amazon.Pages.Cart);
        String item_actual_price = basketAction.selectBasketItem(1).getPriceOfProduct();

        Assert.assertEquals(item_actual_price, item_expected_price, "Expected price: "+item_expected_price+" but actual price was "+item_actual_price);
    }

}
