package utils;

import com.persado.oss.quality.stevia.selenium.core.WebComponent;
import org.apache.commons.logging.Log;
import org.openqa.selenium.StaleElementReferenceException;

public class Helpers extends WebComponent {

    long timeout = 10000;

    public void clickStaleElement(String locator, long timeout) {
        boolean succeded = false;
        long startTimer = System.currentTimeMillis();
        while(!succeded && (System.currentTimeMillis()-startTimer < timeout)) {
            try {
                controller().waitForElement(locator);
                controller().moveToElement(locator);
                controller().click(locator);
                succeded = true;
            }
            catch(StaleElementReferenceException staleRefExeption) { }
            catch(Exception e) { }
        }
    }

}
