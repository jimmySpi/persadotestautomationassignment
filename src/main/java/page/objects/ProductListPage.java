package page.objects;

import com.persado.oss.quality.stevia.selenium.core.WebComponent;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ProductListPage extends WebComponent {

    public enum ProductListPageLocators {

        PRODUCTS("css=a[class*='a-link-normal s-access-detail-page']");

        private String locator;

        ProductListPageLocators(String _locator) {
            locator = _locator;
        }

        public String get() { return locator; }

    }

    public void selectProductFromListAt(int desiredPosition) {
        controller().findElements(ProductListPageLocators.PRODUCTS.get()).get(desiredPosition-1).click();
    }

}
