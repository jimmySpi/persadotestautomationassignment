package page.objects;

import com.persado.oss.quality.stevia.selenium.core.WebComponent;

public class CartPage extends WebComponent {

    public enum CartPageLocators {

        ITEM_DIV("css=div[data-item-count='POSITION']");

        private String locator;

        CartPageLocators(String _locator) {
            locator = _locator;
        }

        public String get() { return locator; }
    }

    public String getItemPriceOfProductAt(int position) {
        return controller().getAttributeValue(CartPageLocators.ITEM_DIV.get().replaceAll("POSITION", String.valueOf(position)), "data-price");
    }
}
