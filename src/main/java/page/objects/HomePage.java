package page.objects;

import com.persado.oss.quality.stevia.selenium.core.WebComponent;

public class HomePage extends WebComponent {

    public enum HomePageLocators {

        IMGBTN_MENS_DRESS_SHOES("css=img[alt*='Shop Men']");

        private String locator;

        HomePageLocators(String _locator) {
            locator = _locator;
        }

        public String get() { return locator; }

    }

    public void clickMensDressShoesImgBtn() {
        controller().press(HomePageLocators.IMGBTN_MENS_DRESS_SHOES.get());
    }

}
