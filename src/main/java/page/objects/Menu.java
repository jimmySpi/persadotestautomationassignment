package page.objects;

import com.persado.oss.quality.stevia.selenium.core.WebComponent;

public class Menu extends WebComponent {

    public enum MenuLocators {

        CART_BTN("css=a[id='nav-cart']");

        private String locator;

        MenuLocators(String _locator) {
            locator = _locator;
        }

        public String get() { return locator; }
    }

    public void goToCart() {
        controller().click(MenuLocators.CART_BTN.get());
    }

}
