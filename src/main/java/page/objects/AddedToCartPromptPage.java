package page.objects;

import com.persado.oss.quality.stevia.selenium.core.WebComponent;

public class AddedToCartPromptPage extends WebComponent {

    public enum AddedToCartPromptPageLocators {

        PRICE_TEXT("css=span[class*='a-color-price hlb-price']");
        private String locator;

        AddedToCartPromptPageLocators(String _locator) {
            locator = _locator;
        }

        public String get() { return locator; }

    }

    public String getPrice() {
        return controller().getText(AddedToCartPromptPageLocators.PRICE_TEXT.get()).substring(1);
    }
}
