package page.objects;

import com.persado.oss.quality.stevia.selenium.core.WebComponent;
import utils.Helpers;

public class ProductPage extends WebComponent {

    protected static Helpers helpers = new Helpers();

    public enum ProductPageLocators {

        SIZE_SELECTBOX("css=select[name='dropdown_selected_size_name']"),
        BTN_ADD_TO_CART("css=span[id='submit.add-to-cart']");

        private String locator;

        ProductPageLocators(String _locator) {
            locator = _locator;
        }

        public String get() { return locator; }

    }

    public void selectSize(String desiredSize) {
        controller().select(ProductPageLocators.SIZE_SELECTBOX.get(), "10.5");
    }

    public void addProductToCart() {
        helpers.clickStaleElement(ProductPageLocators.BTN_ADD_TO_CART.get(), 6000);
    }

}
