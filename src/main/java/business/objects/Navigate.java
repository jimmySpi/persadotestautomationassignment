package business.objects;

import com.persado.oss.quality.stevia.selenium.core.WebComponent;
import org.springframework.beans.factory.annotation.Autowired;
import page.objects.HomePage;
import page.objects.Menu;
import utils.Amazon;


public class Navigate extends WebComponent {

    @Autowired
    protected HomePage homePage;
    @Autowired
    protected Menu menu;

    public void To(Amazon.Pages page) {
        switch (page) {
            case Home:
                controller().navigate("https://www.amazon.com");
            case Mens_Dress_Shoes:
                homePage.clickMensDressShoesImgBtn();
                break;
            case Cart:
                menu.goToCart();
                break;
            default:
                throw new IllegalArgumentException("The parameter ("+page+") given is invalid");
        }
    }

}
