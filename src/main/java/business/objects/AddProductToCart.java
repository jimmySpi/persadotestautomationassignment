package business.objects;


import com.persado.oss.quality.stevia.selenium.core.WebComponent;
import org.springframework.beans.factory.annotation.Autowired;
import page.objects.AddedToCartPromptPage;
import page.objects.ProductListPage;
import page.objects.ProductPage;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class AddProductToCart extends WebComponent {

    @Autowired
    protected ProductListPage productListPage;
    @Autowired
    protected ProductPage productPage;
    @Autowired
    protected AddedToCartPromptPage addedToCartPromptPage;

    public AddProductToCart selectFromListProductAt(int position) {
        productListPage.selectProductFromListAt(position);
        return this;
    }

    public AddProductToCart setSize(String desiredSize) {
        productPage.selectSize(desiredSize);
        return this;
    }

    public AddProductToCart add() {
        productPage.addProductToCart();
        return this;
    }

    public String getPrice() {
        return addedToCartPromptPage.getPrice();
    }

    public AddProductToCart selectFromListProductWithName(String name) {
        throw new NotImplementedException();
    }
    public AddProductToCart setQuantity(int desiredQuantity) {
        throw new NotImplementedException();
    }

}
