package business.objects;

import com.persado.oss.quality.stevia.selenium.core.WebComponent;
import org.springframework.beans.factory.annotation.Autowired;
import page.objects.CartPage;

public class BasketAction extends WebComponent {

    @Autowired
    protected CartPage cartPage;

    private int POSITION = 0;

    public BasketAction selectBasketItem(int position) {
        POSITION = position;
        return this;
    }

    public String getPriceOfProduct() {
        return cartPage.getItemPriceOfProductAt(POSITION);
    }

}
